/**
* 2011/05/07 19:10:37 BigCat Software
* round corners for IE <9
**/

DD_roundies.addRule('.field.field-name-field-summary', '15px');
DD_roundies.addRule('#footer-bottom-left', '10px');
